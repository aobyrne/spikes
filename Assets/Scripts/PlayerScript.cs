﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour {

	enum Status
	{
		Alive,
		Dead
	};

	public float horizontalSpeed = 250f;
	public float verticalSpeed = 360f;
	public float horizontalDirection = 1;

//	private float leftBorder;
//	private float rightBorder;
//	private Vector2 extents;
	private Animator animator;
	private Rigidbody2D rbody;
	private Status status;

	void Start () {

		status = Status.Alive;

		animator = GetComponent<Animator>();
//		animator = GetComponentInParent<Animator>();
		rbody = GetComponent<Rigidbody2D>();

		rbody.freezeRotation = true;
//		rbody.isKinematic = true;
		
//		leftBorder = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0)).x;
//		rightBorder = Camera.main.ViewportToWorldPoint (new Vector2 (1, 0)).x;
//		extents = GetComponent<Renderer> ().bounds.extents;

		// move offscreen
//		Vector3 worldPos = Camera.main.ViewportToWorldPoint (new Vector3 (0.5f, 1.0f, 0));
//		transform.position = new Vector3 (worldPos.x, worldPos.y + 2*extents.y, 0);

	}

	void Update () {

		if (status == Status.Dead)
			return;

        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                flap();
            }
        }

		if (Input.GetKeyDown (KeyCode.G)) {
//			GetComponent<Rigidbody2D> ().isKinematic = false;
			animator.SetTrigger ("playerIdle");
		} else if (Input.GetKeyDown (KeyCode.Space)) {
			flap();
		}

//		if (transform.position.x - extents.x <= leftBorder)
//			flip();
//
//		if (transform.position.x + extents.x >= rightBorder)
//			flip();

	}

	private void OnCollisionExit2D (Collision2D other)
	{
			
		Debug.Log (other.gameObject.tag);

		if (other.gameObject.tag == "WallLeft" || other.gameObject.tag == "WallRight") {
			horizontalDirection *= -1;
			transform.localScale = new Vector2(horizontalDirection, 1);
		}
		
	}

	void flap () {

		animator.SetTrigger ("playerJump");
		rbody.velocity = new Vector2(0, 0);
		rbody.AddForce (new Vector2(horizontalDirection * horizontalSpeed, verticalSpeed));

	}

//	void flip () {
//
//		// track direction
//		horizontalDirection *= -1;
//
//		// position inside game area
//		if (horizontalDirection == 1) {
//			transform.position = new Vector2 (leftBorder + extents.x, transform.position.y);
//		} else {
//			transform.position = new Vector2 (rightBorder - extents.x, transform.position.y);
//		}
//
//		// flip horizontally
//		transform.localScale = new Vector2(horizontalDirection, 1);
//
//		// reverse velocity
//		Vector2 v = GetComponent<Rigidbody2D>().velocity;
//		GetComponent<Rigidbody2D>().velocity = new Vector2(-v.x, v.y);
//
//	}

	public void die() {

		status = Status.Dead;

		rbody.freezeRotation = false;
//		GetComponent<Rigidbody2D> ().isKinematic = true;
		animator.SetTrigger ("playerDeath");

        //		Vector2 v = GetComponent<Rigidbody2D>().velocity;
        //		rbody.velocity = new Vector2(-v.x, v.y);

        //		rbody.velocity = new Vector2(0, 0);
        //		Vector2 v = rbody.velocity;
        //		rbody.AddForce (new Vector2(-v.x, v.y));

		SceneManager.LoadScene("Menu", LoadSceneMode.Additive);

    }
}
