﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreScript : MonoBehaviour {
	
    public Text field;

    void Start () {
		PlayerPrefs.SetInt ("score", 0);
	}

	void Update () {
		field.text = "" + PlayerPrefs.GetInt("score");
	}
}
