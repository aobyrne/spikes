﻿using UnityEngine;
using System.Collections;

public class SpikeScript : MonoBehaviour {

	public float horizontalDirection = 1;

	void Start () {

		transform.localScale = new Vector2(horizontalDirection, 1);

	}

	private void OnCollisionEnter2D (Collision2D other)
	{

		if (other.gameObject.tag == "Player") {
			PlayerScript player = (PlayerScript)other.gameObject.GetComponent<PlayerScript>();
			player.die();
		}

	}
}
