﻿using UnityEngine;
using System.Collections;
using System;

public class GameScript : MonoBehaviour {

	public static GameScript instance;

	public GameObject spikePrefab;
	public GameObject wallPrefab;
    public GameObject coinPrefab;

	private float screenWidth;
	private float screenHeight;

	private float screenTop;
	private float screenBottom;
	private float screenLeft;
	private float screenRight;

	void Awake () {
		instance = this;
	}

	void Start () {

		screenWidth = Camera.main.ViewportToWorldPoint (new Vector2 (1, 0)).x;
		screenHeight = Camera.main.ViewportToWorldPoint (new Vector2 (0, 1)).y * 2;

		screenTop = Camera.main.ViewportToWorldPoint (new Vector2 (0, 1)).y;
		screenBottom = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0)).y;
		screenLeft = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0)).x;
		screenRight = Camera.main.ViewportToWorldPoint (new Vector2 (1, 0)).x;

		CreateWalls();
		CreateSpikes();
        CreateCoins();

	}

    private void CreateCoins()
    {
        float x = UnityEngine.Random.Range(-screenWidth + 1, screenWidth - 1);
        float y = UnityEngine.Random.Range(-screenHeight + 1, screenHeight - 1);
        Instantiate(coinPrefab, new Vector3(x, y, 0), Quaternion.identity);
    }

    void Update () {
		
		if (Input.GetKeyDown (KeyCode.R)) {
			Application.LoadLevel(Application.loadedLevel);
		}
		
	}

	public void CreateWalls() {

		// top
		GameObject wallTop = (GameObject)Instantiate(wallPrefab, new Vector3(0, screenTop, 0), Quaternion.identity);
		wallTop.transform.localScale = new Vector3 (screenWidth + 100, 1, 1); // need to fix this +100 width, wasn't fitting entire screen

		// bottom
		GameObject wallBottom = (GameObject)Instantiate(wallPrefab, new Vector3(0, screenBottom, 0), Quaternion.identity);
		wallBottom.transform.localScale = new Vector3 (screenWidth + 100, 1, 1); // need to fix this +100 width, wasn't fitting entire screen

		// left
		GameObject wallLeft = (GameObject)Instantiate(wallPrefab, new Vector3(screenLeft, 0, 0), Quaternion.Euler(0, 0, 90));
		wallLeft.tag = "WallLeft";
		wallLeft.transform.localScale = new Vector3 (screenHeight, 1, 1);

		// right
		GameObject wallRight = (GameObject)Instantiate(wallPrefab, new Vector3(screenRight, 0, 0), Quaternion.Euler(0, 0, -90));
		wallRight.tag = "WallRight";
		wallRight.transform.localScale = new Vector3 (screenHeight, 1, 1);

	}

	public void CreateSpikes() {

		// remove existing spikes
		GameObject[] spikes = GameObject.FindGameObjectsWithTag ("Spike");
		foreach (GameObject spike in spikes) {
			GameObject.Destroy(spike);
		}

		float spikeWidth = spikePrefab.GetComponent<Renderer>().bounds.size.x;
		float spikeHeight = spikePrefab.GetComponent<Renderer>().bounds.size.y;

		int numSpikes = (int)System.Math.Round (screenHeight / spikeHeight, 0);

		// left column
		for (int i = 0; i < numSpikes; i++)
		{
			if (UnityEngine.Random.Range(0f, 1f) > .8f) {
				Vector3 pos = new Vector3(screenLeft, screenTop - i * spikeHeight, 0);
				Instantiate(spikePrefab, pos, Quaternion.identity);
			}
		}

		// right column
		for (int i = 0; i < numSpikes; i++)
		{
			if (UnityEngine.Random.Range(0f, 1f) > .8f) {
				Vector3 pos = new Vector3(screenRight, screenTop - i * spikeHeight, 0);
				GameObject spikeClone = (GameObject)Instantiate(spikePrefab, pos, Quaternion.identity);
				spikeClone.GetComponent<SpikeScript>().horizontalDirection = -1;
			}
		}

 	}
}
