﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HighScoreScript : MonoBehaviour
{
    public Text field;

    void Start()
    {
		Vector3 screen = Camera.main.ViewportToWorldPoint (new Vector2 (1, 0));
		transform.position = new Vector2(
			screen.x - GetComponent<RectTransform>().rect.width * transform.localScale.x,
			-screen.y - GetComponent<RectTransform>().rect.height * transform.localScale.y
		);

		if (!PlayerPrefs.HasKey("highScore")) {
        	PlayerPrefs.SetInt("highScore", 0);
		}
    }

    void Update()
    {
        field.text = "" + PlayerPrefs.GetInt("highScore");
    }
}
