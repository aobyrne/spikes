﻿using UnityEngine;
using System.Collections;

public class CoinScript : MonoBehaviour {

	private float screenWidth;
	private float screenHeight;
	private Vector3 extents;

	void Start () {

		screenWidth = Camera.main.ViewportToWorldPoint (new Vector2 (1, 0)).x;
		screenHeight = Camera.main.ViewportToWorldPoint (new Vector2 (0, 1)).y;
		extents = GetComponent<Renderer> ().bounds.extents;

		Reposition ();

	}

	private void OnTriggerStay2D (Collider2D other)
	{

		if (other.tag == "Player") {
			Score();
			Reposition();

			GameScript.instance.CreateSpikes();
		}

	}

	private void Score()
	{

		int score = PlayerPrefs.GetInt ("score");
		int highScore = PlayerPrefs.GetInt ("highScore");
		int newScore = score + 1;
		PlayerPrefs.SetInt ("score", newScore);
		if (newScore > highScore) {
			PlayerPrefs.SetInt ("highScore", newScore);
		}
	}

	private void Reposition ()
	{

		float x = Random.Range (-screenWidth + 1, screenWidth - 1);
		float y = Random.Range (-screenHeight + 1, screenHeight - 1);
		transform.position = new Vector2(x, y);

	}
}
